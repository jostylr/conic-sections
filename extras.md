# Extras

This contains the pebbles and graphs for the material.

# Graph plane basic

Put in a graph of the x-y plane with a point (3,4). 

[line]()

Put in a graph of the x-y plane of 2x + 3 = 5 and its intersection.

[parabola]()

put in a graph of x-y plane of x^2 = 9 and its solution. Also one of x^2 -9
=0.    

## Graph pythagoras

Graph a right triangle with side lengths a, b, c. have the formula on it
too as well as a right angle in the triangle. 

Then add a graph of a 3, 4, 5 right triangle.

## Graph distance in plane

A graph with (m,n), (p,q), vertex (p,n), the triangle in dotted lines, the
formula a = m-p,  b = n-q,  c= distance. 

[numbers]()

Have one with numbers

## Math Pebble Distance

A pebble in which one can input two points and get the distance. Input
option of either (x,y) or x y or form for each of the four coordinates.
Output option of different kinds of numbers (Exact, decimal to certain
number of places). Also, below the answer should be a graph of the plane
with the two points on it, the line connecting with length on the line and a
dotted right triangle and its lengths. 

    Math Pebble [Distance](http://mathpebbles.com/distance)

## graph midpoint

A graph of the midpoint of a segment. Have the line segment, the
endpoints, the midpoit, two small triangles between end points and
midpoint as well as the big triangle between endpoints. Note the slopes
and lengths. 

## pebble midpoint

Have a pebble that does that for any two input points. Also have it setup
so that one can input the midpoint and an endpoint and give the other
endpoint. Or put in all three for a right/wrong check within tolerances?


## graph circles

Have a graph of various circles. concentric circles at origin of various
radii. And then a different circle with a different center. 

## Pebble Circles

Implement 5 different ways of specifying circles plus given equation, read
off data. Show the constructive way as well as algebraic way for deriving
the equations. 

## Exercises prerequisites

Solve some lines, quadratics. Read off graphical solutions. Compute
distance, midpoint, between two points. Compute perpendiculars and
bisectors. 

Intersection of two lines? 

Circles:  given center, radius, write down equation; given equation, write
down center, radius. Find circle equation from various different 5 input
types. Given equation in expanded form, transform to standard form.

## graph first parabola

Graph of parabola  y = 4x^2 (or whichever one has focus at (0,1) and
directrix y=-1). 

Put on several points with distances to the focus and directrix. 

## pebble parabola definition

A pebble in which one can drag the focus and directrix around, changing
the parabola. Add points to the parabola to see the distances. 

## graph parabola derivation

A graph with the focus, directrix, point (0,0), and (1, a) on it. Also put
the distances for both points to the given places. 

## graph parabola upside down

This should be a specific parabola with given numbers, but upside down.
otherwise, same view as the previous one. Specifically, y=x^2/-8$ with
focus (0, -2) and directrix y=2. 

## pebble parabola at origin

Have a slider for a and a toggle for vertical vs horizontal. Have the
directrix and focus computed as well as the points of interest. Allow the
putting on of points that automatically have the distances on their.
Display equation. Also make directrix/focus dragggable, effecting a and
the equation. 

## pebble trace out parabola

The
first game, you are given a directrix and focus. Your task is to drag the
point around tracing out the parabola. A correct point will be green,
incorrect points are red. You can also change the directrix and/or focus
and your highlighted points will change color accordingly to this new
parabola.  


## pebble fit parabola

In the second game, you are given a graph of a parabola (and you can
see/set
its equation if you like). Your task is to determine the directrix and
focus. You will have a ghostly parabola corresponding to the parabola
defined by the current directrix and focus. Drag the focus and directrix
around to try and match up the ghost. 


## exercises parabola

* given focus, directrix, write down the equation
* given equation, write down focus and directrix
* given vertex, orientation, and focal length, write down equation

## graph ellipse

A basic graph of an ellipse with all the parts labeled. 

## pebble ellipse 

Drag a point around the given ellipse and see the string length remain
constant. Allow for any ellipse.

## pebble ellipse games

Same basic game as with the parabola. given foci, trace out ellipse,
flashing green or red, depending. Given ellipse, have foci to drag around
with ghost ellipse. 

## graph 2a

Show the graph of the string length being 2a from the vertices and foci

## graph foci location

Graph the location of the foci and the point on the minor axis. Show the
right triangle and the relevant computations.

## pebble point and drag ellipse

Given ellipse equation, draw it and demonstrate axes, vertices, and foci. 

## exercises ellipse

* given foci and string length, write down the equation
* given equation, write down foci and other info
* given vertex, orientation, and focal length, write down equation

## graph basic hyperbola

Graph a basic hyperbola with the various geometric parts of interest
labeled. 

## pebble hyperbola paint game

Given a hyperbola foci, differential (changeable), paint out the hyperbola

## pebble hyperbola ghost game

given equation, align the differential and foci to match

## pebble hyperbola asymptotic game

Given hyperbola, line up the asymptotes. make zoom out easy.

## graph Hyperbola special point

Draw in the special point $(c, ...)$ for the hyperbola and put in the
distance differential pictorially. 

## pebble hyperbola information

A pebble that given an equation, we get out all the hyperbola information. 
Or given information, get the equation and other information.

Maybe something where given partial information it shows possible fits
with the data? 

## exercises hyperbola

* given foci, write down the equation
* given equation, write down foci and other info
* given vertex, orientation, and focal length, write down equation

## Graph of shifted parabolas

Put graphs of various shifted parabolas

## Pebble drag parabola vertex

Create a pebble where one can either drag the vertex and see how the
equations look or can use sliders on the variables and see how the vertex
changes. 

## Pebble synthetic division for completing the square

Have a pebble that just does synthetic division for completing the square

## Pebble drag parabola get formulas

Have draggable parabola stuff and show the formulas aplied and the actual
points on the parabola. Infinite number of examples! have steps available
too. 

## exercises completing the square

* given vertex, write down parabola
* given vertex equation, write down vertex, roots
* given expanded equation, get vertex, roots, vertex form
* doing this with two variables involved




