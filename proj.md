# Conic Sections

This is a book that will cover the needed knowledge about conic sections for
students of Algebra 2 - Calculus II. 

## Hook

Everything you need to know about conic sections

Mastering conic sections

Quick conic section mastery

1 hour conic section mastery

Easy conic sections

Get an A on your conic section test

A level conic sections

Fast conic sections

## Customer Avatar

1. Perfect Penny. Wants in-depth understanding of the material. Very
   superficial course being taught to them. 
   Goal: Understanding, mastery
   Obstacles: Information scattered, incomplete. May not have background for
   all bits.
   Fears: Missing something important.
   Needs: Proofs. 
2. Confused Connie. Wants a good grade. Decent at math, but conic sections
   have blown her mind. 
   Goal: Good enough understanding for an A
   Obstacles: In a state of confusion. All sorts of facts that are hard to
   remember and get straight. Minus, plus? Oh, boy. Not willing to put too
   much time into it. 
   Fears: Not getting a good grade.
   Needs: Practice time.
3. Teaching Tammy. Teacher/tutor who has to learn about conic sections. Has math
   knowledge.
   Goal: Be able to teach them with confidence. Get some nice tricks and
   stories.
   Obstacles: Limited time. A bit rust on the subject. A bit embarassed. 
   Fears: Screwing the topic up. 


## Chapters

    # Doable Conic Sections

    _"introduction"

    _"points"

    _"lines"

    _"parabolas"
    
    _"ellipses"

    _"hyperbola"

    _"completing the square"
    
    _"translations"

    _"unifying relations"

    _"rotations"

    _"quadratic systems"

    _"polar coordinates"

    _"parametrization of curves"

    _"reflective properties of conics"

    _"areas and arcs"

    _"slicing a cone"

    _"actions to take"

Other topics not included: higher dimensional stuff, reflections and other
transformations. 


## Introduction

Here we state, very briefly, the setup.

Why should I read this? 

    ## Introduction

    Conic sections are a good mix of geometric and algebraic knowledge/skills.
    They require you to do both with or translate from one to the other
    quickly and easily. 

    This is hard. This book is here to help with that. 

    I take it in several steps, mimicking in reverse how you might go about
    doing this in .  You need to first learn the basic forms and then one can
    move on to translated and rotated conic sections. You will find with these
    ideas and methods that conic sections become easy to do. 

    Once the basic conic section material is mastered, then we move into some
    extra topics that would be of special interest to those studying physics,
    particularly orbital mechanics, acoustics, and optics. 

    The first portion of this book should help anyone who is studying conic
    sections for the first time. With the included material, you will easily
    improve your grades in your math course. There are online exercises and
    explorations as well as videos that come with this material, all linked to
    conveniently from this book. Use them and conic sections will become
    intuitive and fluid. 

    But this book offers much more than just what is needed in a typical
    Algebra 2 or Precalculus course on conic sections. For those who wish to
    truly undertstand and push the limits, we go through the increasingly
    complicated ways quadratic equations in two variables can appear. By the
    time we are done, any such equation should be easily handled. We also
    handle how to find the intersections of two conic sections.

    That's not the end of it though. We also will see how to appreciate the
    unity of the conic sections, from different perspectives. And the
    applications of conic sections helps explain why this is of practical
    interest to many engineering problems.

    The many exercises, and the generators of such exercises, are free to use
    in any fashion a reader of this book chooses to use. In particular, if you
    are a teacher or a tutor, feel free to use a generated list of problems
    for a quick way to create additional problems, with or without solutions,
    to give your students practice. And if you are a student, enjoy an endless
    number of problems. Do them until you are perfect in the application of
    this material. Though I do recommend thoroughly immersing yourself into a
    few select problems as well as doing drills of these problems. 

### Confused Connie

Will this help me get a good grade? How long will this take?

### Perfect Penny

Does this cover everything I need to know? Will this dumb things down? 

### Teaching Tammy

Does this have good bits that I can use? Will it help me prepare problems,
lessons?

## Points

What do I need to know to understand this material?

* Variable manipulations
* Cartesian coordinates

    ## Points

    What do I assume you know already? This is a fair question to ask at the
    beginning of each math conversation. Some of the later sections will
    require a bit more knowledge (provided with a bit of review here), but
    here these are the core basics needed through. In summary, if you have
    seen what is typically called Algebra I, then that should be sufficient. 

    The most important concept to be familiar with is simply that of variable
    manipulations. Can you solve 2x +3 = 5? Does that equation make sense to
    you? How about  x^2 = 9? Do you know that x = 3 and x=-3 are the two
    solutions to it? 

    Cartesian coordinates are an absolute must to understand as well.

    _"extras::graph plane basic"

    Does the above graph seem familiar? Do you see that the x-coordinate of
    the point is 3 and the y is 4?

## Lines

    ## Lines

    Lines are a conic section that you should already be familiar with. What
    follows below is a brief review of what you should know about lines. 

    _":definition"

    _":perpendicular lines"


    !! write about lines being figures of constant slope?, perpendiculars?

    _"extras::graph plane basic:line" 

    This is showing the graphincal view of solving 2x+3 = 5. We graph the line
    2x +3 and the line y=5. Their intersection is the solution. Note that the
    aglebraic manipulations can also be graphed at each step. For example,
    subtracticing 3 from both sides leads to 2x = 2 and each side can be
    graphed. The x-coordinate of the intersection will be the same in each
    case. 

    _"extras::graph plane basic:parabola"

    We can also graphically solve x^2 = 9. Graph x^2, graph y = 9, find the
    intersection. The form x^2-9 can be graphed and the intersections with the
    x-axis are the solutions as well. 

    If you are comfortable with all of that, then you should be able to follow
    much of this book, certainly the main portion dedicated to Algebra 2 class
    material. 
    
    !! Intersection of two lines?

[distance formula]()

What is the distance between two points?

* Two points given, length of segment between them
* Pythagorean theorem
* formula
* pebble to compute distance

    ### Distance Formula

    The distance formula is of fundamental importance in understanding conic
    sections. As we shall see, the definition of conic sections geometrically
    is entirely in terms of distances. 

    So what is the distance between two points? We use the Pythagorean theorem
    to inform our definition of the distance (there are other possibilities!).
    The Pythagorean theorem states that the length of the hypotenuse in a
    right triangle is related to the lengths of the other two sides in a very
    specific way: 

    \[a^2 + b^2 = c^2\]

    _"extras::graph pythagoras"

    There are many visual proofs of this; see ...

    The distance formula is essentially using this theorem where we imagine
    that given points (m,n) and (p,q), we have a right triangle with vertex
    (p,n). 

    _"extras::graph distance in plane"

    Those are random points giving the formula  

    \[sqrt{(m-p)^2 + (n-q)^2}\]

    And an actual example with numbers.

    _"extras::graph distance in plane:numbers"

    Note that if the two points are either on the same horizontal line or
    vertical line, then the distance collapses to $|m-p|$ or $|n-q|$,
    respectively. 

    Also note that distances are always positive unless the two points are the
    same point, in which case the distance is zero. Distances are never
    negative. 

    If distance is unfamiliar to you, please check out the _"extras::math
    pebble distance"

[Midpoint formula]()

What is the midpoint between two points?

* horizontal, vertical
* combined, average
* length between
* pebble to compute midpoint, difference

    ### Midpoint Formula

    Another useful formula in what follows is that of the midpoint. 

    If you have two numbers, what is the number halfway between them? This is
    the average of the two numbers. For line segment, the midpoint is the
    point whose coordinates are halfway between the respective coordinate
    values. 

    For a line segment with endpoints (m, n) and (p,q), the
    midpoint is the average of the coordinates:  $(\frac{m+p}{2},
    \frac{n+q}{2})$. 

    How can we check this? First verify that the proposed point is on the same
    line as defined by the two given points. This can be done by verifying
    that the slope between the midpoint and the two endpoints are all the
    same. Then we need to check that the distance between the proposed
    midpoint and the two endpoints are equal which implies it is the midpoint. 

    _"extras::graph midpoint"

    _"extras::pebble midpoint"

    !! Perpendiclar bisector? 


## Circles

What is the figure of all points a fixed distance from a given point?

* geometric view
* translating into algebra
* bi-directionaly equivalence reasoning
* three points, two points on a diameter, two points + radius, one point
  + center
* pebble to draw circles in various above ways

    ## Circles

    While this is a conic section, odds are that you have seen the circle
    before. It is a special case in many ways, but it is also a nice simple
    case to start looking at the tools and notions we will be using. 

    ### Circle Definition

    So what is a circle? It is the set of all points in the plane that are a
    given distance away (the radius) from a given point (the center). So a
    circle is defined by a distance and a center. 

    _"extras::graph circles"

    ### Algebraic Equation for Circles

    Much of our concern will be in dealing with the algebraic side of these
    figures. So how do we get an equation for a circle? Let's start simple. We
    shall start with a circle whose center is at the origin. 

    Let's say the distance is r. Then we want all the points (x,y) such that
    the distance from the orign to the point is r: 

    \[  r = \sqrt{ (x-0)^2 + (y-0)^2} \]

    Squaring both sides and simplifying, we get the simple equation $x^2 + y^2
    = r^2$. 

    This is a direct application of the distance formula. And since I
    conveniently placed the 0's in the distance formula, perhaps you can see
    how to modify it for a different center?

    Indeed, we replace the 0's with the appropriate coordinates: 

    \[ (x-h)^2 + (y-k)^2 = r^2 \]

    where (h, k) is the center.

    As an example, the circle with center (3, -2) and radius 2 is defined by
    the equation $(x-3)^2 + (y+2)^2 = 4$. The claim is that if (x,y) satisfies
    that equation, then it is on the desired circle. And if a point is on the
    desired circle, then it satisfies the equation. Understand that both
    statements need to be true for an algebraic equation to claim to have
    captured the geomtric figure entirely. 

    Tip: See how the signs are different between the center's coordinates and
    what appears in the formula? The way to remember it is that if we had a
    circle of zero radius, then the only point that would satisfy that is
    (h,k). So in the formula, inputting (h,k) should lead to the left-hand
    side being 0. See that it does. 

    ### Specifying a Circle

    Circles have many wonderful properties, but we will not delve into them
    here. But before we move on into the new conic sections, we should note
    that there are several ways to specify a circle:

    * center, radius.  We have seen this already. 
    * Endpoints of a diameter. A diameter is a line segment going through the
      center of the circle with endpoints on the circle. The midpoint is the
      center and half the length of a diameter is the radius. So this quickly
      becomes the first case. 
    * Two points plus the radius. Geometrically, we can draw in two circles
      with the given radius. They should intersect in two points ( two
      possible circles), one point (they are endpoints of the diameter of the
      circle), or none (there is no such circle). Algebraically, we can input
      the two points in the circle equation along with the radius and solve
      for h and k, ending up with two possible solutions. Constructively, we
      can use the method for three points below, using the radius as the
      length of the perpendicular segment to the midpoint of the line segment
      connecting the two points (see below).
    * One point with center. The distance from the point to the center is the
      radius.
    * Three points. We can do this algebraically by using the circle equation
      and solving for h, k, and r. We have three unknowns and we will have
      three equations from each of the three points. So that should work out.
      Another way, constructively, is to use the geometric fact that the
      radius will perpendicularly bisect a chord (a line segment with endpoints on the
      circle) So we can find the midpoints and the perpendicular lines through
      those points. Then we find the intersection of those lines.

    _"extras::pebble circles"

    ### Three points on a circle

    Let us do the three point construction in detail and then try the same
    computation with pure algebra. This is difficult and may be safely
    skipped. 

    We will use an actual set of three points. (Choose something nice!) 

    ....

    ### Expanded form

    What if we have an equation of a circle that has been expanded out? That
    is $(x-2)^2 + (y+1)^2 = 4$ could be written as $x^2 -4x + 4 + y^2 +2y + 1
    = 4$ or even $2x^2 -8x + 2y^2 +4y + 2 = 0$. How do we get the standard form
    in which we can just read off the center and radius? How do we recognize
    that this is a circle? 

    That it is a circle comes from the fact that we have equal coefficients in
    fron of the square terms. So the first step is divide by that coefficient
    so that we have $x^2$ and $y^2$ only: $x^2 -4x + y^2 +2y +1 = 0$.

    Next, we complete the square. We will discuss this in more generality in
    the translation section, but basically it is a method by which we generate
    the square terms we are seeking. For example, $y^2 + 2y = y^2 + 2y + 1 -1
    = (y+1)^2 - 1$ where the method tells you what constant to add/subtract.
    Again, see the secion on completing the square if you wish to know now. 

    ### Exercises

    _":exercises"
    

[exercises]()

What are some problems related to this?

    _"extras::exercises prerequisites"

## Parabolas

What is a parabola?

* picture 
* definition geometrically

    ## Parabolas

    You are most likely already familiar with parabolas of the form 
    $y = ax^2 +bx + c$. These are parabolas that either open upwards ($a>0$)
    or downwards ($a<0$). Note that $a=0$ leads to a line. 

    This form of a parabola is usually introduced as simply the next step
    after lines. Lines we have x to the power 1 and so we increase the power
    to 2. Then we have cubics, quartics, and polynomials of higher degrees. 

    But parabolas naturally arise in the context of conic sections. They have
    a natural geometric definition. Namely, given a point called the focus and
    a line called the directrix, the related parabola is the set of all points
    whose distance from the focus is the same as the distance to the
    directrix. 

    The vertex of the parabola is the unique point that is on the
    parabola and on the perpendicular line to the directrix that contains the
    focus. This happens to also be the point that is closest to the focus and
    directrix. 

    _"extras::graph first parabola"

    ### Algebraic Formulation

    _":algebraic formula"

    ### Equivalence Proof

    _":geometry vs algebra"

    ### Exercises

    _"extras::exercises parabola"


[Algebraic formula]()

How do I get the information from the equation?

* directrix
* focus
* x vs y
* pebble: given equation, spit out information with pictures

    The geometric definition above works for all parabolas in the plane. Any
    line and any point not on that line will serve as a directrix and focus,
    respectively. 

    But we want the algebraic definition. And it will take us some time to get
    the full version algebraically under control. Before we begin with our
    algebraic derivations, a graphical game may be fun to experiment with. 
    
    The
    first game, you are given a directrix and focus. Your task is to drag the
    point around tracing out the parabola. A correct point will be green,
    incorrect points are red. You can also change the directrix and/or focus
    and your highlighted points will change color accordingly to this new
    parabola.  

    _"extras::pebble trace out parabola"

    In the second game, you are given a graph of a parabola (and you can
    see/set
    its equation if you like). Your task is to determine the directrix and
    focus. You will have a ghostly parabola corresponding to the parabola
    defined by the current directrix and focus. Drag the focus and directrix
    around to try and match up the ghost. 

    _"extras::pebble fit parabola"
    
    
    We shall start with two
    simple types of parabolas: vertical ones and horizontal ones. And even
    among those types, we shall start with those whose vertices are at the
    origin, $(0,0)$. 

    The vertical parabolas have equations of the form $y = a x^2$. They either
    open upwards if $a>0$ or downwards if $a<0$. 

    The directrix will be a horizontal line and the focus will be a point on
    the y-axis. What are they?  To answer that, we need a point other than the
    vertex. The vertex, along with the vertical orientation, has already told
    us that the driectrix is horizontal (orientation) and the vertex is on the
    y-axis as the y-axis is the perpendicular line through the vertex. 

    So let's pick a convenient point, say $(1, a)$. Then we need to find the
    distance to the vertex $(0,p)$ and the directrix $y=-p$.  Why are they
    equal, but opposite? That's because the distance to the vertex to either
    is the same and that leads to this --- improve!.
    
    _"extras::graph parabola derivation"

    The distance to
    the directrix is the perpendicular distance to it from the point. Thus, it
    is $|-p-a|$. This should be equal to the distance to the focus: 
    $\sqrt{ (1-0)^2 + (a-p)^2}$. 

    That is, we need to solve

    \[(-p-a)^2 = 1^2 + (a-p)^2\]

    Expanding, we have

    \[ p^2 + 2ap + a^2 = 1 + a^2 -2ap +p^2 \]

    The squared terms cancel and we are left with 

    \[ 4ap = 1\] 

    which is to say

    \[ p = \frac{1}{4a} \]

    Thus, the directrix is  $y=-\frac{1}{4a}$ and the focus is at
    $(0,\frac{1}{4a})$. 

    As an example, let's compute the focus and direcrix of $y=\frac{-1}{8} x^2$. The
    formulas above tell us the directrix is $y=-\frac{-8}{4} = 2$ while the
    focus is (0, -2)$. This parabola opens downwards and is relatively wide. 

    _"extras::graph parabola upside down"

    ---

    What if we had parabolas of the form $x = a y^2$?  These open right ($a>0$)
    or left ($a<0$). The directrix will be $x=-\frac{1}{4a}$ and the focus
    will be $(\frac{1}{4a}, 0)$. The computations show this by a simple
    switching of $x$ and $y$ in the derivation. 

    ---

    _"extras::pebble parabola at origin"


[geometry vs algebra]()

How is the geometric related to the algebraic?

* given geometry, what is the equation
* given equation, show geometry does work

    The above assumed that the algebraic equation and geometric definition
    coincided. Given that assumption, we showed how the equation gives the
    focus and directrix. 

    Let us now show definitively that these two definitions agree. 

    Given a point (x,y) on the graph $y=ax^2$, is it equally distant to the
    purported focus $(0, \frac{1}{4a})$ and directrix $y = \frac{-1}{4a}$ ?

    We compute the distances and see that they are the same. The point, using
    the graph equation, is $(x, ax^2)$. The distance to the directrix is
    $|ax^2 + \frac{1}{4a}|$. The distance to the focus is 
    \[ \sqrt{(x-0)^2 + (ax^2-\frac{1}{4a})^2}\]

    To show these are equivalent is the same as showing 
    
    \[ (ax^2 + \frac{1}{4a})^2 =  x^2 + (ax^2 - \frac{1}{4a})^2 \]

    We expand the left and then add in $0 = x^2 - 2 \frac{2a x^2}{4a}$ ...
    this leads to the right hand-side. 

    Thus, given a point on the graph, it is a point on the parabola defined by
    the directrix and focus. 

    If we have point that is on the parabola, does it satisfy the equation? 

    So we have a point $(x,y)$ whose distance to the vertex and directrix is
    given. That is, we know it satsifies

    \[ (y + \frac{1}{4a})^2 =  x^2 + (y-\frac{1}{4a})^2 \]

    Expanding and simplifying (both the $y^2$ and the square of the focal
    distance cancel), we have 

    \[ \frac{2y}{4a} =  x^2 - \frac{2y}{4a} \]

    Combining and simplifying we get $\frac{y}{a} = x^2\] which is equivalent
    to $y = ax^2$. 

    So indeed these do describe the same objects. 

[exercises]()

What are some problems related to this?

* given focus, directrix, write down the equation
* given equation, write down focus and directrix
* given vertex, orientation, and focal length, write down equation


## Ellipses


What is a ellipse?

* definition, picture

    ## Ellipse 

    Ellipses are rather like squashed circles. They have two foci and two
    directrices though we will save the directrix discussion for later. 

    The basic geometric definition is that we define the ellipse by choosing
    the foci and a number I call the string length. A point is on the ellipse
    exactly when the sum of the distances to the foci from the point is the
    string length. 

    The center of the ellipse is the midpoint between the two foci. The foci
    define the major axis. The two points of the ellipse that lay on this
    major axis are the vertices. The line through the center and perpendicular
    to the major axis is the minor axis. The intersections of the minor axis
    with the ellipse is often asked for as well and I call them the minor
    vertices. The focal length is the distance between the foci? or focus and
    center? or focus and vertex?

    _"extras::graph ellipse" 

    _"extras::pebble ellipse"

    We can also play the same kind of game with ellipses that we did with
    parabolas: 

    _"extras::pebble ellipse games"

    ### Algebraic Formulation

    _":algebraic formula"

    ### Geometry vs. Algebra

    _":geometry vs algebra"

    ### Exercises

    _"extras::exercises parabola"


[Algebraic formula]()

How do I get the information from the equation?

* vertices, minor, major, 
* foci 
* directrix 
* x vs y
* pebble: given equation, spit out information with pictures

    Just as with parabolas, we will take it slow to work up to the full
    ellipses algebraically. For now we will discuss ellipses centered at the
    origin and with either horizontal or vertical major axis. 

    Let's start with the horizontal. We claim that the equation describing
    such ellipses is

    \[ \frac{x^2}{a^2} + \frac{y^2}{b^2} =  1 , a>b>0\]

    Accepting this for the moment, how do we get the ellipse's information?  
    Let's start easy. What are the vertices? The major axis is the x-axis. So
    $y=0$. Plugging that into the equation, we get  $\frac{x^2}{a^2} = 1$ or
    $x^2 = a^2$.  Therefore, the vertices are $(a, 0)$ and $(-a, 0)$.  The
    minor vertices occur when $x=0$ and so, similarly, $(0, b)$ and $(0, -b)$
    are the minor vertices. 

    The string length is $2a$ which follows from the symmetric placement of
    the foci on the line containing the vertices. Indeed, the foci
    are at $(c, 0)$ and $(-c, 0)$ due to the assumption that the ellipse's
    major axis is the x-axis and the center is at $(0,0)$. So then the
    distance to $(a,0)$ is $|a-c|$ and $|a-(-c)|$, respectively. Summing
    (using $a>c>0$), we have $|a-c|+|a+c| = a-c+a+c=2a$. 

    _"extras::graph 2a"

    To get the foci (what the value of $c$ is), 
    let's consider the symmetrically placed $(0,b)$. The
    length is from the foci to $(0,b)$ is given by the Pythagorean theorem as
    $\sqrt{c^2 + b^2}$. This is true of the distance from both foci so we
    know from the constancy of the string length that $2 \sqrt{c^2+b^2} = 2a$
    This tells us that $c^2 + b^2 = a^2$. Therefore $c = sqrt{a^2 - b^2}$. In
    summary, the foci are at $(\pm \sqrt{a^2-b^2}, 0)$.

    Note that for this to work out, $a^2$ better be larger than $b^2$. This is
    a consistency check with the assumption that the major axis is, in fact,
    horizontal. 

    _"extras::graph foci location"   

    If $b>a$, then the ellipse is oriented vertically. The same computations
    lead to the major axis being vertical with vertices $(0,\pm b)$.
    The minor vertices are $(\pm a, 0)$. And the foci are then at
    $(0, \pm \sqrt{b^2-a^2})$

    Here we have an issue of notation. From the perspective of horizontal vs.
    vertical, it is natural to condition on $a$ compared with $b$ while
    leaving $a$ paired with $x$ and $b$ paired with $y$. 

    But from the perspective of general ellipses (rotated ones, particularly),
    it is natural to have $a$ associated with the major axis. And this is what
    you will often find in textbooks. 

    My advice is to compute both sets of vertices and see which one provides
    the greatest distance between the vertices. This will give you the maor
    axis. If you have followed the derivation of the $c$, it should be no
    trouble to remember how to derive it in any of the circumstances. 
    
    _":extras::pebble point and drag ellipse"

[geometry vs algebra]()

How is the geometric related to the algebraic?

* given geometry, what is the equation
* given equation, show geometry does work
* pebble: draw in foci, get ellipse equation. trace out ellipse using
  geometry

    The above presupposes that the equation does describe an ellipse. We
    should see why this is true. 

    So given a point $(x,y)$ on the ellipse with foci $(\pm c,0)$ and string
    length $s$, does it
    satisfy the given equation $x^2/a^2 + y^2/b^2 = 1$ where $a = s/2$ and $b
    = \sqrt{a^2-c^2}$. 

    We know from the defintion that $\sqrt{(x-c)^2 + y^2} + \sqrt{(x+c)^2 +
    y^2} = s ...  maybe replace s with 2a. tried to do the algebra. don't see
    how it works out. 

    Should get  $x^2/a^2 + y^2/(a^2 -c^2) = 1$ and need to argue using
    triangle inequality and sum condition that $a>c$. 

    The same algebra can be reversed to show that a point satisfying the
    equation is a point on the ellipse. 

    Thus, the set of points satisfying the equation is the exact same set as
    the set of points satisfying the ellipse definition. 

    Maybe the directrix and focus relation might be easier to work with? Need
    to establish that the directrix-focus relation is the same geometrically. 

    Add some graphs for this proof. 

[exercises]()

What are some problems related to this?

* given foci, write down the equation
* given equation, write down foci and other info
* given vertex, orientation, and focal length, write down equation
* given three points on ellipse, get equation/info


## Hyperbolas

What is a hyperbola?

    ## Hyperbolas 

    We have one final conic section to discuss: hyperbolas.

    The flow should be familiar by now. The geometric defintion is the set of
    points that satisfy the absolute difference of the distances between a
    given point
    and the two foci is a constant, which I shall call the differential
    length.

    The axis of symmetry is the line defined by the two foci. The midpoint
    between the foci is the center. The vertices of the hyperbola is where the
    axis meets the hyperbola. In contrast to the ellipse, the vertices are
    between the foci. 

    Hyperbolas have infinite extent, similar to parabolas. But hyperbolas tend
    to look like lines far away from the center. These lines are the 
    asymptotes. The lines pass through the center. The slope is easy to
    compute from the given algebraic equation for the hyperbola. 

    _"extras::graph basic hyperbola"

    As before, we will discuss the equivalent algebraic formulation followed
    by some musings on the equivalence. 

    But first, try out the hyperbola games:

    _"extras::pebble hyperbola paint game"

    _"extras::pebble hyperbola ghost game"

    _"extras::pebble hyperbola asymptotic game"

    ## Algebraic Formulation

    _":algebraic formula"

    ## Geometry vs Algebra

    _":geometry vs algebra"

    ## Exercises

    _"extras::exercises hyperbola"

[Algebraic formula]()

How do I get the information from the equation?

* vertices, center
* foci 
* asymptotes
* directrix 
* x vs y
* pebble: given equation, spit out information with pictures

    We will assume that the center of the hyperbola is at $(0,0)$ and the axis
    of symmetry is horizontal (later vertical). 

    We claim that the equation that describes an asymptote is 

    \[ \frac{x^2}{a^2} - \frac{y^2}{b^2} = 1 \]

    Note that we have a difference instead of a sum here, which is a nice
    homage to the geometric definition and its differential. 

    Also note that there is no longer a need for $a>b$. They can have any
    relation. What determines the axis is which of the terms is positive when
    put in this standard form of having a $+1$ on the right-hand side. 

    To get the vertices from this equation, we set $y=0$. This immediately
    yields $x = \pm a$ and so the vertices are at $(a, 0)$ and $(-a, 0)$. 

    With ellipses, we also set $x=0$. Doing that here leads to an equation
    that cannot be solved with real numbers. In particular, there are no
    points of the hyperbola that touch the y-axis.

    The asymptotes are easy to find if we do a little rearranging. Since we
    are interested in the scenario of being far away from the origin, we have
    $x$ is massive, that is, $x^2 >> 0$.  Notice that since the difference has
    to be 1, $y^2$ must also be very massive. 

    So let's divide by $\frac{x^2}{b^2}$.  This gives us the equivalent
    equation (for $x \neq 0$) : 

    \[  \frac{b^2}{a^2} - \frac{y^2}{x^}2 =  \frac{b^2}{x^2} \]

    Since $x^2$ is very large (think a million times $b^2$),  we see that the
    term on the right is approximately $0$. Rearranging terms accordingly, we
    have 

    \[\frac{y^2}{x^2} =  \frac{b^2}{a^2} \]

    Taking square roots, we see that the asymptotes satisfy

    \[y = \pm \frac{b}{a}  x \]

    And that is the equation of the asymptote. 

    What about the foci? Just like the other conic sections, we need another
    point other than a vertex. But there does not seem to be any natural
    point. Or is there?

    The foci can be assumed to be at $(\pm c, 0)$ as they are symmetrically
    placed around the center (origin) and along the assumed horizontal axis,
    that is, the x-axis. 

    The differential length can be computed from the vertex $(a, 0)$. Indeed,
    the distance to the foci would be $c-a$ and $c+a$. So their difference is
    2a, the same as with the ellipse. 

    Natural points to consider on the hyperbola are where nice stuff happens.
    One possible point is where $x=c$. The $y$ coordinate, picking the
    positive one for convenience, is $\frac{b}{a} \sqrt{c^2 - a^2} = u$.

    _"extras::graph hyperbola special point"

    The differential constancy requirement leads to $\sqrt{(2c)^2 + u^2} - u =
    2a$. We get rid of the square root by moving u to the other side and
    squaring. So we have  $(2c)^2 + u^2 =  4a^2 + 4ua + u^2$. The u^2 cancel.
    Remember $u$ has a square root. So we isolate that as well, getting 
    $4ua = 4a^2 - 4c^2= 4 (c^2 - a^2)$. 

    Replacing $u$ with what it is, we have  $4a\frac{b}{a} \sqrt{c^2-a^2} = 4
    (\sqrt{c^2-a^2})^2$ which reduces to $b = \sqrt{c^2-a^2}$. Squaring and
    rearranging, we have 

    \[ c= \sqrt{b^2 + a^2} \]

    Notice that instead of a difference, we have a sum of the squares of $a$
    and $b$. 

    And that is how to tease out all the information. 

    "extras::pebble hyperbola information"


[geometry vs algebra]()

How is the geometric related to the algebraic?

* given geometry, what is the equation
* given equation, show geometry does work

    Set it up, but probably need to defer to directrix stuff? 


[exercises]()

What are some problems related to this?

* given foci, write down the equation
* given equation, write down foci and other info
* given vertex, orientation, and focal length, write down equation



## Completing the Square

How do I complete the square?

* Explain the goal

    ## Completing the Square

    We have seen the equation for the conic sections when they are suitably
    rooted at the origin (center at the origin for ellipse and hyperbola,
    vertex at origin for parabola). 

    The next step is to tackle conic sections where those special points are
    at an arbitrary point in the plane. They will still be oriented either
    horizontally or vertically. 

    In order to do the computations, we need to review the method of
    completing the square. It is absolutely essential that you have this
    technique mastered for the following chapter. 

    The method I present below is my personal way of remembering how to
    complete the square. Their are many ways to accomplish this. I prefer this
    way as I can step through it with very little formulaic memorizing.

    ### Up and Down

    _":up and down"

    ### Vertex halfway

    _":vertex halfway"

    ### Plugging in

    _":computing the y-vertex"
    
    ### Write it down

    _":writing down the square"

    ### Formulas

    _":formula"

    ### Exercises

    _":exercises"


[up and down]()

* Give examples of a single shifted parabola up and down, then left and
  right. 
* Demonstrate location of roots, vertices
* Pebble: drag parabola around and see relevant information change

    The first step is a little bit of visual exploration and how the vertex
    and roots change. 

    !! Have a graph for each one; extras::graph of shifted prabolas:...!!

    Let's start with $y=x^2$. It's vertex is $(0,0)$ and it has one double
    root at the vertex.

    If we add a constant, say $y = x^2 + 4$, then the parabola shifts upwards
    by 4 units: vertex $(0,4)$ and there are no roots. 
    
    If we subtract one, say $x^2 - 9$, then the parabola shifts
    downwards by 9 units: vertex $(0, -9)$ and roots $(3,0)$ and $(-3,0)$.

    Those are the vertical shifts. All of them produce vertices on the y-axis.
    The downwards ones will have roots with the midpoint at the origin. 

    Another kind of shift are the left-right shifts. We can replace $x$ with
    $(x-h)$. 

    For example, $y = (x-2)^2$ will have the vertex at $(2,0)$. This is a
    shift of the graph to the right. Notice the opposite sign of the vertex
    from the shift constant. The way to think about is to look for where the
    square is 0. 

    For $y=(x+3)^2,  we get a vertex of $(-3, 0)$. 

    We can combine these two operations. The general quadratic can be written
    as $y = (x-h)^2 + k$ where $(h, k)$ is the vertex. 

    For example, $y = (x-2)^2 - 9$ has vertex $(2, 9)$ and roots $(-1, 0)$ and
    $(5,0)$. Notice that the roots are shifted to the right by 2 when compared
    to those of $y=x^2 - 9$. Also notice that the vertex is still halfway
    between the roots. The equation can be expanded to $y = x^2 -4x -5$. Our
    task will be to see how to go from the expanded form to the standard form
    of the vertices.

    _"extras::pebble drag parabola vertex"


[Vertex halfway]()

How do I get the x-coord of the vertex

* Vertex is halfway between roots
* x-coord vertex is unchanged by shifting
* ignore constant; then halfway is easy to see.

    The first step in completing the square is to accept the following facts:

    * The vertex is halfway between the roots. 
    * If we shift the parabola up or down, the x-coordinate of the vertex is
      unchanged. 

    ?? establish these facts ??

    If you accept these, then to find the x-coordinate of the vertex is as
    simple as ignoring the constant. 

    Indeed, if we have  $y = ax^2 + bx + c$,  then the vertex will have the
    same x-coordinate as the parabola $y = ax^2 + bx = x(ax+b)$. The roots of
    this are clearly $x=0$ and $x=-\frac{b}{a}$.  Halfway between them is
    $x=-\frac{b}{2a}$. 

    As an example, $y = x^2 -4x -5$ has its truncated partner as $y=x^2 -4x =
    x(x-4)$ whose roots are $0$ and $4$. Halfway is $2$. 


[Computing the y-vertex]()

How do I efficiently compute the y coord of the vertex?

* y-coord is evaluate the parabola at the x-coord
* horner's algorithm is quicker? Check 

    The next step is fairly easy. We take the x-coordinate and plug it in to
    the original equation. 

    So in our example, we know the vertex is $(2, ??)$. That question mark can
    be determined by evaluating the parabola at $x=2$. Here $y = 2^2 - 4*2 - 5
    = 4-8-5 = -9$. So the vertex is $(2,-9)$. 

    A method known as Horner's methord aka synthetic division, may be quicker.
    In that method adapted here, we write:

    2 | 1 -4 -5

    And then we bring down the 1, multiply by it 2, put it under the -4, add,
    multiply by 2, put it under the -5, add, bring down and write the answer: 

    2 | 1 -4 -5
           2  -4
    -------------
        1  2  -9

    Notice that -9 at the end? This is our y-coordinate of the vertex!

    !! need to put this in math form and ideally generated from synthetic
    division pebble. Ideally, we could have a pebble that does this over and
    over. 

    _"extras::pebble synthetic division for completing the square"


[Writing down the square]()

How do I write down the square of the equation?

* given x, y coords of vertex, equation is ...
* expand out to check
* bonus: roots are ...

    At this point, we know how to get the vertex of the quadratic. Given the
    vertex $(h,k)$, the vertex form (also standard form, square form) is
    $y=a(x-h)^2 + k$

    Notice that it is $-h$ because we want that term to be zero at the vertex
    to get a $y$ value of $k$. 

    In our example,  the vertex is $(2, -9)$. So we can immediately write down
    the form $y = (x-2)^2 -9$.

    Given this vertex form, the roots are also easy to find. Replace $y$ with
    $0$. Then we end up with, in our example, $9 = (x-2)^2$. Take square roots
    to get $\pm 3 = x-2$ and $x = 5$ or $x=-1$.

    In general, the root is  $h \pm \sqrt(-k)$  This fits very well with the
    vertex being halfway between the roots. 

[Formula]()

Is there a formula I can just memorize?

* vertex: (-b/2a, (b^2-4ac)/4 )  ?
* square: a(x-b/2)^2 + (b^2-4ac)/4  ?
* roots: quadratic
* pebble:  illustrate all of this with draggable parabola parameters and
  pictures.

    What about formulas? Everybody loves a good formula, right? Well, I find
    the sign issues to be a little dubious and prefer thinking it through each
    time. 

    But if you like, here are the formulas given the parabola $y=ax^2 + bx +
    c$

    * vertex: $(-\frac{b}{2a}, -\frac{b^2}{4a} + c)$
    * square:  $ a (x+\frac{b}{2a})^2 - \frac{b^2}{4a} + c)$
    * roots:  $-\frac{b}{2a} \pm \sqrt{\frac{b^2}{4a} - c}$ which is more
      conventionally written as $\frac{-b \pm \sqrt{b^2 -4ac}{2a}$.

    _"extras::pebble drag parabola get formulas"


[exercises]()

What are some problems related to this?

* given vertex, write down parabola
* given vertex equation, write down vertex, roots
* given expanded equation, get vertex, roots, vertex form
* doing this with two variables involved



## Translations

What about conic sections that are not at the origin?

* demonstrate the equation form in standard form
* show what it looks like in expanded form. 
* pebble that converts from one form to the other.

    ## Translations

    description of the flow: complete the square, replace with u and v. 

    ### point, line, circles?

    ### ellipses

    ### hyperolas

    ### parabolas

    ### exercises

    The goal of this section is to handle translated conic sections. Here are
    examples of what the equations look like

    * parabola:  $y = 2(x-4)^2 + 9$
    * parabola: $x = -3(y-5)^2 -7$
    * ellipse: $\frac{(x+3)^2}{4} + \frac{(y-2)^2}{9} = 1$
    * hyperbola: $\frac{(x+3)^2}{4} - \frac{(y-2)^2}{9} = 1$
     
    In each of those examples, if we think of replacing the squared
    expressions with variables $u$ and $v$, we can see a form familiar to us. 

    * parabola:  $v = 2u^2$ ($u=x-4$, $v=y-9$)
    * parabola: $u = -3v^2$ ($u=x+7$, $v = y-5$)
    * ellipse: $\frac{u^2}{4} + \frac{v^2}{9} = 1$ where $u=x+3$ and $v=y-2$
    * hyperbola: $\frac{u^2}{4} - \frac{v^2}{9} = 1$ where $u=x+3$ and
      $v=y-2$
     
    You can then apply the information we learned earlier to get the
    information in terms of $u$ and $v$. The final step is to translate back
    into $x$ and $y$ coordinates. 

    * parabola:  $v = 2u^2$ ($u=x-4$, $v=y-9$). Vertex: $(0,0)$, 
    Focus: $(0,\frac{1}{8})$, directrix
      $v=-\frac{1}{8}$. Translated into x,y: Vertex: $(4, 9)$, Focus: $(4,
      9+\frac{1}{8})$, Directrix: $y = 9 -\frac{1}{8}$
    * parabola: $u = -3v^2$ ($u=x+7$, $v = y-5$). 
    * ellipse: $\frac{u^2}{4} + \frac{v^2}{9} = 1$ where $u=x+3$ and $v=y-2$
    * hyperbola: $\frac{u^2}{4} - \frac{v^2}{9} = 1$ where $u=x+3$ and
      $v=y-2$
     
     .... this is too hurried. Need to expand all of this. 

    Each of these have expanded forms. 

    ...

    Our task is to learn how to convert the expanded forms into standard form
    and then read off the information. 

    ### Reading off information

    _":reading off information"

    ### Completing the square

    _":completing the square"




[completing the square]()

How do I get the equation into standard form?

* complete the square
* get = 1

    

[reading off information]()

How do I use the standard form?

* center is to be read off
* u,v coordinates for replacing center. write down info
* replace with center shift

[formula]()

Is there a formula I can just memorize?

* figure out a formula for each? Apply complete the square formula
* ugh.

[exercises]()

What are some problems related to this?

* read off info from standard form
* expand standard
* obtain standard form
* get info and draw pictures (standard, expanded, bastardized standard
  form (completed the square, but not 1 on the right, say -- tricksies) )

## Unifying relations

Is there something that relates these things?

* directrix in each type
* directrix, foci length
* note similiarities in ellipse and hyperbola formula
* pebble: input conic, show directrix, foci, lengths
* pebble: draw in other lines as directrix and see its failure, slide
  into actual one.

[eccentricity]()

What is the eccentricity of a conic section?

* the ratio of proportionality of distance
* formulas

[classifying]()

What are the different types of eccentricities?

* show the eccentricities of different ratios
* give the classification
* circle as "0"
* equal coeffcient hyperbola is what? (square root 2) Wouldn't that be circle analog? Perhaps not. 

[imaginary]()

Hyperbolas and ellipses seem to have very similar information but with
minus signs. Is there a way to relate them?

* Write formulas and highlight comparsion
* Square root of -1, anyone?
* Complex sphere picture -- valid? pebble??
* Circles again? 

[exercises]()

What are some problems related to this?

* compute eccentricity given various forms (directrix+focus, equations
  standard and then expanded) and classify
* A little bit with the imaginary stuff

## Rotations

What if a conic is rotated?

* pictures of rotated conics
* equations to go with those pictures
* pebble of all of that; spin the conic!

[angles]()

How do we compute angles?

* A little bit of right angle trigonometry
* inverse trig functions used in computing angles
* pebble of point in plane and the associated angle
* side note on radians and all of this, but keep in degrees

[rotations geometrically]()

What are the rotated conics geometrically?

* rotated definitional objects
* expanding into equations

[algebra]()

How do we understand the algebraic version?

* rotating between two standard forms (x major vs y major)
* simple x-y hyperbola derivation -- rotation angle, square form, (u,v)
* generalizing procedure, substituting variables -- different graph
  pictures, overlaid
* pebble: given equation, get rotation angle and square form

[formulas]()

Is there a formula I can use?

* tan/cot stuff
* formula vs procedure?

[rotations and translations]()

How do I deal with rotated and translated conics?

* no idea. experiment, pebble: rotate, translate, see effect on
  coordinates
* is there a way to see what one should do with each?
* goal is to get in u,v coordinates the simple, basic form
* maybe we can tease out directrix, focus in some other way?

[exercises]()

What are some problems related to this?

* given a rotation and a basic system, what is the algebraic equation?
* given an algebraic equation, what is the conic and rotation angle?
* translate/rotate combo stuff??

## Quadratic Systems

How do we solve intersections of conic systems?

* given a system of quad/linear equations, what are the solutions?
* pebble: given a system, draw graphs and their intersection points.
  Make figures draggable so one can see how types of solutions change;
  rotation and translation. Also conic changeable in various ways (and how
  one becomes the other).

[line and line]()

How do we find the intersection of two lines? 0, 1, oo

* simple linear systems
* substitution
* cancellation
* degenerate cases: geometric meaning

[line and conic]()

A line intersects a conic section in 0, 1, 2 points. How do we know/find
them?

* tangent, secant
* center conic at origin?
* solve algebraically

[conic and conic]()

And the intersection of a conic with a conic? 0, 1, 2, 4, oo (same one)

* start with co-centered at origin, and also rotated ones.
* one at origin, one not
* neither at origin (translate one to the origin)?
* purely algebraic cancellation stuff -- cancel a square, solve for the
  other square? 

[geometry]()

What is the relation to some geometric facts?

* circle intersects circle, tangent lines, etc...??

[exercises]()

What are some problems related to this?

* Solve a bunch of systems, graph them

## Polar coordinates

How are conics represented in terms of distance and angle from origin?

[polar coordinates]()

How do you represent a point in terms of distance and angle?

* (x,y). draw in triangle, show r, theta
* formula for conversion
* restriction on r, theta
* graphs in r, theta vs x,y vs. graphing on the x,y plane using r,theta
  descriptions

[circles]()

What is a circle in polar coordinates?

* r is fixed. pretty easy
* translated circle

[lines]()

What is a line in polar coordinates?

* through the origin
* translated

[conics]()

What is the general conic in polar coordinates?

* demonstrate process
* get nice formula involve e

[translations]()

What happens if we are not centered at the origin in all of this?

* conics translated

[other polar curves]()

What else can we graph easily in polar coordinates?

* some pretty pictures

[exercises]()

What are some problems related to this?

* convert various conics and stuff to polar
* given polar, what is cartesian and/or info

## Parametrization of curves

How do I have a point travelling on the conic section?

* orbital motions trace out conics
* how do we model motion along a conic?
* pebble: tracing out conics, variable speed, the equations
* general (x(t), y(t) ) setup

[parabola]()

How do I parametrize? What can I say about the curves
tangent vector?

* parametric form, cartesian, polar
* tangent vector relation to radial
* acceleration vector relations

[ellipse]()

How do I parametrize? What can I say about the curves
tangent vector?

* parametric form, cartesian, polar
* tangent vector relation to radial
* acceleration vector relations

[hyperbola]()

How do I parametrize? What can I say about the curves
tangent vector?

* parametric form, cartesian, polar
* tangent vector relation to radial
* acceleration vector relations

[kepler]()

So what did Kepler do again?

* discuss orbits
* kepler's laws as laws of ellipses

[exercises]()

What are some problems related to this?

* write parametric equations of these things
* do some orbital computations

## Reflective properties of conics

How do light and sound reflect off of conics?

* basic behavior of light, sound: angle of reflection from a surface is angle
  relative to tangent line of surface. 
* need to talk a bit about tangent lines

[parabolic mirror]()

How do we focus with parabola?

* lines coming in perpendicular to directrix go to focus. 
* light emitting from focus reflect into perpendicular lines?
* examples, proof
* hitting external part of parabola?

[hyperbolic mirrors]()

How do we focus with hyperbola?

* external light towards one focus bounces to the other 

[elliptic mirrors]()

How do we focus with ellipses?

* light from one focus bounces to the other

[reflecting telescope]()

How is a reflecting telescope built?

* parabolic mirror to hyperbolic to elliptic. 

[whispers]()

How do whisper points work?

* ellipsoid -- 3d version
* whisper at location then it goes out to all other placees and reflects
  from focus to other focus
* gathering all sounds from other places focuses them into a larger sound
* time delays?
* what about light?

[exercises]()

What are some problems related to this?

* do some computations of lines reflecting, tangent lines

## Areas and arcs

How do we compute areas and/or arc length?

* No idea. Need to figure this all out

* The hope would be that a sequence of simple areas could be used for all
  of them to get some nice formulas. something like the parabola example
  with a line cutitng off an area leading to a parallel tangent line and
  thus defining the h. 

* What about arc length of these figures?

[exercises]()

What are some problems related to this?

* presumable compute the areas.

## Slicing a cone

How are conics related to the slice of cones?

* gotta have the classic picture
* discuss a bit about cones and their properties
* why do we care?
* pebble with a cone and plane on one side and the image on the plane on the
  other. 

[geometric answer]()

How are the geometric definitions related to the cone slices?

* point is plane through center, perpendicular to axis
* line is plane through center, containing axis?
* circle is plane perpendicular to axis
* ellipse is plane tilted to axis
* hyperbola is plane parallel to axis
* parabola is plane tilted relative to axis

?

[algebraic answer]()

How do the algebraic formulas relate to the cone slices?

* compute the formula of an example from each of these. try to do it
  simply. 


[exercises]()

What are some problems related to this?

* do computations

## Actions to take

What do I do next?

* congratulations! you made it through

[confused connie]()

What to step through for practice problems and formulas

* highlight some pebbles and exercises of basic nature

[perfect penny]() 

What are some ideas and considerations for future pursuits?

* emphasize some of the more interesting bits
* higher dimensional stuff, reflections, shears

[teaching tammy]()

What are some good activites for classrooms? What are the pain points?

* stories to tell, challenge level prerscription?
